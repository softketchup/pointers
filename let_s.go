package main

import (
	"fmt"
)

func main() {

	// fmt.Println("Hoi, Go!")
	// for i := 0; i < 5; i++ {
	// 	fmt.Println(i)
	// }

	first := 100
	//var second *int = &first
	second := &first // указатель

	first++
	*second++

	third := &second // указатель на указатель
	**third++

	fourth := &third // указатель на указатель указателя
	***fourth++

	fmt.Println("First: ", first)
	fmt.Println("Second (adres): ", second)
	fmt.Println("Second (value): ", *second)
	fmt.Println("Third (value): ", **third)
	fmt.Printf("Four + 1 (value): %d", ***fourth+1)
}
